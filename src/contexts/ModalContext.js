import React from 'react';
import { signup, signin } from '../auth';
import { menu } from '../auth/menu';
import 'react-notifications/lib/notifications.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import _ from 'lodash';
import { profileOrder, profileReview } from '../auth/profile';

//defined modal popup context
const ModalContext = React.createContext();

class ModalProvider extends React.Component {
  state = {
    openModal: false,
    switches: true,
    heading: "SignUp",
    s_name: "",
    s_email: "",
    s_pass: "",
    l_email: "",
    l_pass: "",
    signupdone: false,
    menus: [],
    category: [],
    profileOrders: [],
    profileReviews: [],
    cartOpen: false,
    cartItem: [],
    getCart: [],
    cartTotal: 0,
    srch: '',
    checkOut:false,
    signUpSwitch:false
  }

  openCheckOut=()=>{
    this.setState({checkOut:true})
  }
  closeCheckOut=()=>{
    this.setState({checkOut:false})
  }
  componentDidMount() {
    //fetching menu
    this.fetchMenu();

    //fetching users orders
    profileOrder().then(result => {
      this.setState({ profileOrders: result });
    }).catch(console.log);

    //fetching users reviews
    profileReview().then(review => {
      this.setState({ profileReviews: review })
    }).catch(console.log);
  }

  //menu fetching

  fetchMenu = () => {
    menu().then(data => {
      this.setState({ menus: data, category: _.uniq(data.map((data) => { return data.labels })) });
    }).catch(console.log);
  }
  getName = (event) => {
    this.setState({ s_name: event.target.value });
  }

  getEmail = (event) => {
    this.setState({ s_email: event.target.value });
  }

  getPassword = (event) => {
    this.setState({ s_pass: event.target.value });
  }

  getLoginEmail = (event) => {
    this.setState({ l_email: event.target.value });
  }

  getLoginPassword = (event) => {
    this.setState({ l_pass: event.target.value });
  }

  onSignUp = () => {
    const { s_name, s_email, s_pass } = this.state;
    const user = {
      name: s_name,
      email: s_email,
      password: s_pass
    };
    signup(user).then(data => {
      if (data.status === 'success') {
        this.setState({
          err: "",
          s_name: "",
          s_email: "",
          s_pass: "",
          signupdone: true
        });
      } else {
        NotificationManager.error(`${data.error}`);
      }
    });
  };

  onReload = () => {
    setTimeout(function () {
      window.location.reload()
    }, 500);
  }
  onSignIn = () => {
    const { l_email, l_pass } = this.state;
    const user = {
      email: l_email,
      password: l_pass
    };
    signin(user).then(data => {
      if (data.status === 'success') {
        localStorage.setItem("user", data.result[0].name);
        localStorage.setItem("userId", data.result[0]._id);
        localStorage.setItem("email", data.result[0].email);
        localStorage.setItem("token", data.token);
        localStorage.setItem("loggedin", true);
        this.handleModal();
        //method called to refresh page
        this.onReload();
      } else {
        //validation shows notification of errors
        NotificationManager.error(`${data.error}`);
      }
    });
  };

  handleModal = () => {
    this.setState({ openModal: !this.state.openModal, switches: true });
  }

  handleForm = () => {
    this.setState({ switches: false, heading: "SignIn" });
  }

  //cart related functions

  openCart = () => {
    this.setState({ cartOpen: true });
  }

  closeCart = () => {
    this.setState({ cartOpen: false });
  }

  //add to cart functions

  onSelectMenu = (menuId, menu_name, menu_price, food_type, quantity) => {
    document.getElementById(menuId).style.background = '#3aac68';
    document.getElementById(menuId).innerHTML = 'Added to cart';
    document.getElementById(menuId).style.textTransform = 'capitalize';

    const foodItem = {
      menuId,
      menu_name,
      menu_price,
      food_type,
      quantity
    };

    const existing = this.state.cartItem.filter(d => d.menuId === menuId);
    if (existing.length > 0) {
      return true
    } else {
      this.setState({ cartItem: [...this.state.cartItem, foodItem], cartTotal: this.state.cartTotal + foodItem.menu_price });
    }
  }

  // getCartItems = () => {
  //   localStorage.setItem('cart', JSON.stringify(this.state.cartItem));
  //   const data = JSON.parse(localStorage.getItem('cart'));   
  //   this.setState({getCart:data})
  // }

  //decrement menu Item
  decrementItem = (menuId) => {
    const existing = this.state.cartItem.filter(item => item.menuId === menuId);
    if (existing[0].quantity > 1) {
      const withoutExisting = this.state.cartItem.filter(item => item.menuId !== menuId);
      const updateUnits = {
        ...existing[0],
        quantity: existing[0].quantity - 1
      };

      this.setState({
        cartItem: [...withoutExisting, updateUnits], cartTotal: this.state.cartTotal - existing[0].menu_price
      });
    } else {
      let array = [...this.state.cartItem];
      let i = array.indexOf(existing[0]);
      if (i !== -1) {
        document.getElementById(menuId).style.background = '';
        document.getElementById(menuId).innerHTML = 'Add to cart';
        document.getElementById(menuId).style.textTransform = 'capitalize';
        array.splice(i, 1);
        this.setState({ cartItem: array, cartTotal: this.state.cartTotal - existing[0].menu_price });
      }
    }
  }

  //increment menu item
  incrementItem = (menuId) => {
    const existing = this.state.cartItem.filter(item => item.menuId === menuId);
    const withoutExisting = this.state.cartItem.filter(item => item.menuId !== menuId);
    const updateUnits = {
      ...existing[0],
      quantity: existing[0].quantity + 1
    };

    this.setState({
      cartItem: [...withoutExisting, updateUnits], cartTotal: this.state.cartTotal + existing[0].menu_price
    });
  }

  //search Menu

  searchMenuItem = (e) => {
    this.setState({ srch: e.target.value })
  }

  //place order
  placeOrder = () => {
    console.log(this.state.cartItem);
    console.log(this.state.cartTotal);
    this.openCheckOut();
    this.closeCart();
  }

  //login switch checkout

  onLoginSwitch=()=>{
    this.setState({signUpSwitch:!this.state.signUpSwitch})
  }
  render() {
    return (
      <ModalContext.Provider value={{
        ...this.state,
        handleModal: this.handleModal,
        handleForm: this.handleForm,
        getName: this.getName,
        getEmail: this.getEmail,
        getPassword: this.getPassword,
        onSignUp: this.onSignUp,
        getLoginEmail: this.getLoginEmail,
        getLoginPassword: this.getLoginPassword,
        onSignIn: this.onSignIn,
        openCart: this.openCart,
        closeCart: this.closeCart,
        onSelectMenu: this.onSelectMenu,
        decrementItem: this.decrementItem,
        incrementItem: this.incrementItem,
        searchMenuItem: this.searchMenuItem,
        placeOrder:this.placeOrder,
        onHideBtn:this.onHideBtn,
        openCheckOut:this.openCheckOut,
        closeCheckOut:this.closeCheckOut,
        onLoginSwitch:this.onLoginSwitch
      }}
      >
        {this.props.children}
        <NotificationContainer />
      </ModalContext.Provider>
    )
  }
}

export { ModalProvider, ModalContext }