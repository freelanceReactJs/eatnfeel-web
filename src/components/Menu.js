import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import styled from 'styled-components';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import RestaurantMenuIcon from '@material-ui/icons/RestaurantMenu';
import { ModalContext } from '../contexts/ModalContext';
import { FaRupeeSign } from 'react-icons/fa';
import img from '../images/food.jpg';
import fnf from '../images/sad.png';

export default class Menu extends React.Component {

  render() {
    return (
      <ModalContext.Consumer>
        {
          value => {
            const { menus, category, onSelectMenu, srch,AddBtn } = value;
            const filterData = menus.filter(data => {
              return (
                data.name.toLowerCase().includes(srch.trim().toLowerCase())
              )
            })
            return (
              <>
                <MenuWrapper>
                  {category.map((data, key) => {
                    return <>
                      <span component="div" className="list-head" hidden={srch.trim().length > 0 ? true : false}> <RestaurantMenuIcon className="icons" /> {data}</span>
                      {filterData.map((menu, key) => {
                        if (menu.labels === data) {
                          return <>
                            <Card className="card" elevation="0">
                              <CardMedia
                                className="media"
                                image={img}
                                title={menu.description}
                              />
                              <CardHeader
                                title={menu.name}
                                subheader={menu.food_type}
                              />
                              <CardActions className="action">
                                <label>
                                  {menu.price} <FaRupeeSign />
                                </label>
                                <Button id={menu._id} variant="contained" size="small" color="primary" onClick={() => onSelectMenu(menu._id, menu.name, menu.price, menu.food_type, 1)}>Add to Cart</Button>
                                {/* <ButtonGroup color="primary" variant="contained" size="small" aria-label="small outlined button group" hidden={!AddBtn }>
                                  <Button>-</Button>
                                  <Button>1</Button>
                                  <Button>+</Button>
                                </ButtonGroup> */}
                              </CardActions>
                            </Card>
                          </>
                        }
                      })}
                    </>
                  })}

                  <div hidden={filterData.length <= 0 ? false : true} className="food-search">
                    <img src={fnf} alt="food not found" />
                    <p>Food not Found.</p>
                  </div>
                </MenuWrapper>
              </>
            );
          }
        }
      </ModalContext.Consumer>
    );
  }
}

const MenuWrapper = styled.div`
width:90%;
display:flex;
flex-wrap:wrap;
margin:90px auto;
    .card{
        width:350px;
        margin:20px;
    }
    .media{
        height:0;
        padding-top:56.25%;
    }
    .avatar{
        background:red;
    }
    .action{
        display:flex;
        justify-content:space-between;
    }
    .list-head{
        width:100%;
        margin:0 20px;
        color:rgb(83, 82, 94);
        font-size: 24px;
        font-weight:600;
        text-transform: capitalize;
        border-bottom: 1px solid rgba(0, 0, 0,0.1);
        padding: 40px 0 10px 0;
        display:flex;
        align-items:center;
    }
    .icons{
        color:#000;
        padding-right:5px;
    }
    .MuiTypography-h5{
        font-size:18px;
      text-transform:capitalize;

    }
    .MuiCardActions-root{
      padding:8px 16px;
    }
    .MuiButton-label {
      text-transform:capitalize;      
    }
    .food-search{
      width:100%;
      margin-top:50px;
      text-align:center;
    }
`