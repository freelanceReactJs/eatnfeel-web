import React from 'react';
import { ModalContext } from '../contexts/ModalContext';
import { Modal, Button, Form } from 'react-bootstrap';

export default class SingInUp extends React.Component {
  render() {
    return (
      <ModalContext.Consumer>
        {
          value => {
            const {
              openModal,
              handleModal,
              switches,
              heading,
              handleForm,
              getEmail,
              getName,
              getPassword,
              onSignUp,
              signupdone,
              getLoginEmail,
              getLoginPassword,
              onSignIn
            } = value;
            return (
              <Modal show={openModal} onHide={handleModal} centered>
                <Modal.Header closeButton>
                  <Modal.Title>{heading}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  {switches ?
                    <Form>
                      <Form.Group controlId="formBasicEmail">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter name" size="sm" onChange={getName} />
                      </Form.Group>
                      <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" size="sm" onChange={getEmail} />
                      </Form.Group>
                      <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" size="sm" onChange={getPassword} />
                      </Form.Group>
                      <Button variant="primary" onClick={onSignUp} size="sm">
                        Register
                   </Button>
                      <Form.Text className="text-muted">
                        Already have an account <Button variant="link" size="sm" onClick={handleForm}>Login</Button>
                      </Form.Text>
                    </Form> :
                    <Form>
                      <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" size="sm" onChange={getLoginEmail} />
                      </Form.Group>
                      <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" size="sm" onChange={getLoginPassword} />
                      </Form.Group>
                      <Button variant="primary" onClick={onSignIn} size="sm">
                        Login
                      </Button>
                      <Form.Text className="text-muted">
                        <Button variant="link" size="sm">Forget your password?</Button>
                      </Form.Text>
                    </Form>
                  }
                </Modal.Body>
                <div
                  className="alert alert-info"
                  style={{
                    display: signupdone ? "" : "none"
                  }}
                >
                  Account created successfully. Please Login!!!
                </div>
              </Modal>
            );
          }
        }
      </ModalContext.Consumer>
    );
  }
}
