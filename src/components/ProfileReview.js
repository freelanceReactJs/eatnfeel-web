import React from 'react';
import { ModalContext } from '../contexts/ModalContext';
import { Card, Button, Container, Row, Col } from 'react-bootstrap';
import styled from 'styled-components';
import Profile from '../components/Profile';
import { FaEdit, FaTrash } from "react-icons/fa";

export default class ProfileReview extends React.Component {
  render() {
    return (
      <ModalContext.Consumer>
        {
          value => {
            const { profileReviews } = value;
            return (
              <>
                <Profile />
                <OrderWrapper>
                  <Container>
                    <Row>
                      <Col sm={12}>
                        <div className="profiler">
                          {profileReviews.map((data, key) => {
                            return (
                              <Card className='cards' key={key}>
                                <Card.Body>
                                  <Card.Title className="titles"><small>{data.reviewDate.substring(0, 10)}</small></Card.Title>
                                  <Card.Title className="span">Rating: {data.rating}</Card.Title>
                                  <Card.Text className="status">{data.comment}</Card.Text>
                                  <ul className="uls">
                                    <li className="lists"><Button variant="info" size="sm"><FaEdit/></Button></li>
                                    <li className="lists"><Button variant="danger" size="sm"><FaTrash/></Button></li>
                                  </ul>
                                </Card.Body>
                              </Card>
                            );
                          })}
                          {profileReviews.map((data, key) => {
                            return (
                              <Card className='cards' key={key}>
                                <Card.Body>
                                  <Card.Title className="titles"><small>{data.reviewDate.substring(0, 10)}</small></Card.Title>
                                  <Card.Title className="span">Rating: {data.rating}</Card.Title>
                                  <Card.Text className="status">{data.comment}</Card.Text>
                                  <ul className="uls">
                                    <li className="lists"><Button variant="info" size="sm"><FaEdit/></Button></li>
                                    <li className="lists"><Button variant="danger" size="sm"><FaTrash/></Button></li>
                                  </ul>
                                </Card.Body>
                              </Card>
                            );
                          })}
                        </div>
                      </Col>
                    </Row>
                  </Container>
                </OrderWrapper>
              </>
            )
          }
        }
      </ModalContext.Consumer>
    )
  }
}

const OrderWrapper = styled.div`
  .profiler{
    background:#fff;
    box-shadow:0 0 2px 2px rgba(0,0,0,0.05);
    border-radius:5px;
    display:flex;
    justify-content:center;
    flex-wrap:wrap;
    margin:10px 0px;
  }
  .cards{
    border-radius:7px;
    width:80%;
    margin:10px auto;
  }
  .status{
    text-transform:capitalize;
    color:#3b3d3c;
    font:14px "Times New Roman";
  }
  .titles{
    text-align:left;
    text-transform:capitalize;
    color:#3B3A3A;
    font:bold 16px "Times New Roman";
  }
  .span{
    text-align:left;
    font:bold 16px "Times New Roman";
  }
  .lists{
    margin: 4px;
    list-style-type:none;
  }
  .uls{
    display:flex;
    justify-content:flex-start;
    padding:0;
    margin:0;
  }
  @media only screen and (max-width:768px){
    .profiler{
      justify-content:center;
    }
    .cards{
      width:300px;
    }
  }
`