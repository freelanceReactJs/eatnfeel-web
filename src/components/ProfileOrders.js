import React from 'react';
import { ModalContext } from '../contexts/ModalContext';
import { Card, Button, Container, Row, Col } from 'react-bootstrap';
import styled from 'styled-components';
import Profile from '../components/Profile';
import { FaRupeeSign } from 'react-icons/fa';

export default class ProfileOrders extends React.Component {
  render() {
    return (
      <ModalContext.Consumer>
        {
          value => {
            const { profileOrders } = value;
            return (
              <>
                <Profile />
                <OrderWrapper>
                  <Container>
                    <Row>
                      <Col sm={12}>
                        <div className="profiler">
                          {profileOrders.map((data, key) => {
                            return (
                              <Card className='cards' key={key}>
                                <Card.Body>
                                  <Card.Title className="titles">Ordered On: <span>{data.dateOfOrder.substring(0, 10)}</span></Card.Title>
                                  <Card.Title className="span"><FaRupeeSign /> {data.total}</Card.Title>
                                  <Card.Text className="status">{data.status}</Card.Text>
                                  <ul className="uls">
                                    <li className="lists"><Button variant="info" size="sm">View Details</Button></li>
                                    <li className="lists"><Button variant="primary" size="sm">Re-order</Button></li>
                                  </ul>
                                </Card.Body>
                              </Card>
                            );
                          })}
                        </div>
                      </Col>
                    </Row>
                  </Container>
                </OrderWrapper>
              </>
            )
          }
        }
      </ModalContext.Consumer>
    )
  }
}

const OrderWrapper = styled.div`
  .profiler{
    background:#fff;
    box-shadow:0 0 2px 2px rgba(0,0,0,0.05);
    border-radius:5px;
    display:flex;
    align-items:center;
    flex-wrap:wrap;
    margin:10px 0px;
  }
  .cards{
    border-radius:7px;
    width:250px;
    margin:10px 15px 25px 12px;
  }
  .status{
    text-transform:capitalize;
    color:#3b3d3c;
    font:14px "Times New Roman";
  }
  .titles{
    text-align:left;
    text-transform:capitalize;
    color:#3B3A3A;
    font:bold 16px "Times New Roman";
  }
  .span{
    text-align:left;
    font:bold 16px "Times New Roman";
  }
  .lists{
    margin: 0 10px;
    list-style-type:none;
  }
  .uls{
    display:flex;
    justify-content:space-between;
    padding:0;
    margin:0;
  }
  @media only screen and (max-width:768px){
    .profiler{
      justify-content:center;
    }
    .cards{
      width:300px;
    }
  }
`