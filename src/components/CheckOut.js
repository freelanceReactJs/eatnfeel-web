import React from 'react';
import styled from 'styled-components';
import { ModalContext } from '../contexts/ModalContext';
import TextField from '@material-ui/core/TextField';
import { Button, FormControl, InputLabel, OutlinedInput, FormLabel, RadioGroup, FormControlLabel, Radio, ButtonGroup } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { FaRupeeSign } from 'react-icons/fa';
import Fab from '@material-ui/core/Fab';
import empty from '../images/emptycart.png';

export default class CheckOut extends React.Component {

    render() {
        const Transition = React.forwardRef(function Transition(props, ref) {
            return <Slide direction="down" ref={ref} {...props} />;
        });
        return (
            <ModalContext.Consumer>
                {
                    value => {
                        const {
                            closeCheckOut,
                            checkOut,
                            onLoginSwitch,
                            signUpSwitch,
                            cartItem,
                            cartTotal,
                            placeOrder,
                            incrementItem,
                            decrementItem
                        } = value
                        return (
                            <>
                                <Dialog fullScreen open={checkOut} onClose={closeCheckOut} TransitionComponent={Transition} color="red">
                                    <CheckoutWrapper>
                                        <AppBar className="appBar" color="default">
                                            <Toolbar >
                                                <IconButton edge="start" color="default" onClick={closeCheckOut} aria-label="close">
                                                    <CloseIcon />
                                                </IconButton>
                                            </Toolbar>
                                        </AppBar>
                                        <div style={{ display: "flex" }}>
                                            <div style={{ width: "100%" }}>
                                                <ProfileWrapper hidden={localStorage.getItem('loggedin')!=null || localStorage.getItem('loggedin')?false:true}>
                                                    <div className="headings">
                                                        Hi, {localStorage.getItem('user')}
                                            </div>
                                                </ProfileWrapper>
                                                <ProfileWrapper hidden={localStorage.getItem('loggedin')==null || localStorage.getItem('loggedin')?true:false}>
                                                    <div className="headings">
                                                        Login Or Sign Up
                                            </div>
                                                    <div hidden={signUpSwitch}>
                                                        <div className="login">
                                                            <TextField
                                                                id="outlined-email-input"
                                                                label="Email Address"
                                                                className="text-field"
                                                                type="email"
                                                                name="email"
                                                                autoComplete="email"
                                                                margin="dense"
                                                                variant="outlined"
                                                            />
                                                            <TextField
                                                                id="outlined-email-input"
                                                                label="Password"
                                                                className="text-field"
                                                                type="password"
                                                                name="password"
                                                                autoComplete="email"
                                                                margin="dense"
                                                                variant="outlined"
                                                            />
                                                            <div className="actions">
                                                                <Button variant="contained" color="primary" >login</Button>
                                                                <div>Not yet Registered? <span className="spans" onClick={onLoginSwitch}>Sign Up</span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div hidden={!signUpSwitch}>
                                                        <div className="login">
                                                            <TextField
                                                                id="outlined-name-input"
                                                                label="Name"
                                                                className="text-field"
                                                                type="text"
                                                                name="c_name"
                                                                autoComplete="text"
                                                                margin="dense"
                                                                variant="outlined"
                                                            />
                                                            <TextField
                                                                id="outlined-mobile-input"
                                                                label="10 digit Mobile Number"
                                                                className="text-field"
                                                                type="text"
                                                                name="mobile"
                                                                margin="dense"
                                                                variant="outlined"
                                                            />
                                                            <TextField
                                                                id="outlined-email-input"
                                                                label="Email Address"
                                                                className="text-field"
                                                                type="email"
                                                                name="email"
                                                                autoComplete="email"
                                                                margin="dense"
                                                                variant="outlined"
                                                            />
                                                            <TextField
                                                                id="outlined-password-input"
                                                                label="Password"
                                                                className="text-field"
                                                                type="password"
                                                                name="password"
                                                                autoComplete="email"
                                                                margin="dense"
                                                                variant="outlined"
                                                            />

                                                            <div className="actions">
                                                                <Button variant="contained" color="primary" >Sign up</Button>
                                                                <div>Already Registered? <span className="spans" onClick={onLoginSwitch}>Login</span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ProfileWrapper>
                                                <ProfileWrapper>
                                                    <div className="headings">
                                                        Delivery Address
                                            </div>
                                                    <div className="login">
                                                        <TextField
                                                            id="outlined-locality-input"
                                                            label="Landmark"
                                                            className="text-field"
                                                            type="text"
                                                            name="area"
                                                            autoComplete="text"
                                                            margin="dense"
                                                            variant="outlined"
                                                        />
                                                        <TextField
                                                            id="outlined-mobile-input"
                                                            label="Street, Building name, House flat no."
                                                            className="text-field"
                                                            type="text"
                                                            name="street"
                                                            margin="dense"
                                                            variant="outlined"
                                                        />
                                                        <TextField
                                                            id="outlined-email-input"
                                                            label="City"
                                                            className="text-field"
                                                            type="text"
                                                            name="pin"
                                                            autoComplete="email"
                                                            margin="dense"
                                                            variant="outlined"
                                                        />

                                                        <FormControl component="fieldset" className="text-field">
                                                            <FormLabel component="legend">Type of Address</FormLabel>
                                                            <RadioGroup aria-label="position" name="position" value={value} row>

                                                                <FormControlLabel
                                                                    value="home"
                                                                    control={<Radio color="primary" />}
                                                                    label="Home"
                                                                    labelPlacement="start"
                                                                />
                                                                <FormControlLabel
                                                                    value="office"
                                                                    control={<Radio color="primary" />}
                                                                    label="Office"
                                                                    labelPlacement="start"
                                                                />
                                                            </RadioGroup>
                                                        </FormControl>
                                                        <div className="actions">
                                                            <Button variant="contained" color="primary" >Save</Button>
                                                        </div>
                                                    </div>
                                                </ProfileWrapper>
                                            </div>
                                            <div style={{ width: "100%" }}>
                                                <ProfileWrapper hidden={cartTotal <= 0 ? true : false}>
                                                    <div className="headings">
                                                        Delivery Time
                                                    </div>
                                                    <div className="login">
                                                        <span>It will take nearly 45min to delivered.</span>
                                                    </div>
                                                </ProfileWrapper>
                                                <ProfileWrapper hidden={cartTotal <= 0 ? true : false}>

                                                    <CouponWrapper>
                                                        <input type="text" placeholder="<%> Apply Coupon code" />
                                                        <Button variant="contained" color="default">Apply</Button>
                                                    </CouponWrapper>

                                                    {cartItem.map((data, key) => {
                                                        return (
                                                            <div className="cart-list">
                                                                <div className="list-item" style={{ width: "10%" }} hidden={data.food_type === 'veg' ? false : true}>
                                                                    <div className='veg-icons'><span className="veg-dot"></span></div>
                                                                </div>
                                                                <div className="list-item" style={{ width: "10%" }} hidden={data.food_type === 'non-veg' ? false : true}>
                                                                    <div className='non-veg-icons'><span className="non-veg-dot"></span></div>
                                                                </div>
                                                                <div className="list-item" style={{ width: "60%" }}>{data.menu_name}</div>
                                                                {/* <div className="list-item" style={{ width: "20%" }}><FaRupeeSign/> 120 </div> */}
                                                                <div className="list-item" style={{ width: "20%" }}>
                                                                    <ButtonGroup size="small" aria-label="small outlined button group">
                                                                        <Button onClick={() => decrementItem(data.menuId)}>-</Button>
                                                                        <Button>{data.quantity}</Button>
                                                                        <Button onClick={() => incrementItem(data.menuId)}>+</Button>
                                                                    </ButtonGroup>
                                                                </div>

                                                                <div className="list-item price"><FaRupeeSign />{data.menu_price * data.quantity} </div>
                                                            </div>)
                                                    })}
                                                    <div className="extra-taxes">
                                                        <div>Delivery Charge</div>
                                                        <div><FaRupeeSign /> 30</div>
                                                    </div>
                                                    <div className="extra-taxes">
                                                        <div>GST 5%</div>
                                                        <div><FaRupeeSign />{(5 / 100) * cartTotal}</div>
                                                    </div>
                                                    <div>
                                                        <Fab variant="extended" color="primary" aria-label="add" size="medium" onClick={placeOrder}>
                                                            Place Order <FaRupeeSign />{cartTotal + 30 + (5 / 100) * cartTotal}
                                                        </Fab>
                                                    </div>
                                                </ProfileWrapper>
                                                <EmptyCart hidden={cartTotal > 0 ? true : false}>
                                                    <img className="imgs" src={empty} alt="empty cart" />
                                                    <div className="headings">
                                                        Your Cart is Epmty
                                                    </div>
                                                </EmptyCart>
                                            </div>
                                        </div>
                                    </CheckoutWrapper>
                                </Dialog>
                            </>
                        )
                    }
                }
            </ModalContext.Consumer>
        )
    }
}

const CheckoutWrapper = styled.div`
width:80%;
margin:75px auto 0;
`
const ProfileWrapper = styled.div`
    width:95%;
    border-radius:2px;
    margin:20px 10px;
    background:#fff;
    box-shadow:0 0 2px 2px rgba(0,0,0,0.05);
    .headings{
        width:100%;
        padding: 20px;
        font-weight: 600;
        font-size: 18px;
        color:#666;
    }
    .login{
        width: 80%;
        box-sizing:border-box;
        margin:auto;
    }
    .text-field{
        width:100%;
        margin:10px 0;
    }
    .MuiInputLabel-outlined,
    .MuiOutlinedInput-input{
        font-size:14px;
    }
    .actions{
        display:flex;
        flex-wrap:wrap;
        align-items:baseline;
        justify-content:space-between;
        Button{
        margin:5px 0px 20px 0;
        }
    }
    .spans{
        cursor: pointer;
        border-bottom: 1px solid;
        color: #3f51b5;
    }
    .headings{
        width:100%;
        padding:10px 20px 0 20px;
        font-family: ProximaNova-Bold,Helvetica,Arial,sans-serif;
        font-weight: 700;
        font-size: 20px;
        color:#555;
    }
    .items{
        font-size:12px;
        padding:5px 20px;
        font-family:ProximaNova-Bold,Helvetica,Arial,sans-serif;
        color:#555;
        display:flex;
        align-items:center;
        border-bottom:1px solid rgba(0,0,0,0.05);
    }
    .veg-icons{
        width:11px;
        height:11px;
        position:absolute;
        border:1px solid green;
    }
    .veg-dot{
            width:5px;
            height:5px;
            border-radius:50%;
            background:green;
            margin:2px;
        position:absolute;
    }
    .non-veg-icons{
        width:11px;
        height:11px;
        position:absolute;
        border:1px solid red;
    }
    .non-veg-dot{
            width:5px;
            height:5px;
            border-radius:50%;
            background:red;
            margin:2px;
        position:absolute;
    }
    .cart-list{
        display:flex;
        flex-wrap:wrap;
        align-items:center;
        padding:10px 20px;
        border-bottom:1px solid rgba(0,0,0,0.05);
    }
    .list-item{
        font-size:16px;
        color:#333;
        margin:0 5px;
        text-transform:capitalize;
    }
    .price{
        font-size:18px;
        font-weight:700;
        width:100%;
        padding:0 55px;
    }
    .MuiButtonGroup-grouped {
        min-width: 30px;
        border-radius: 20px;
        padding:2px;
        outline-style:none;
    }
    .imgs{
        width:100px;
        border-bottom: 1px solid rgba(0,0,0,0.05)
    }
    .items-likes{
        display: flex;
        align-items: center;
    }
    .items-likes-content{
        border:none;
        width:100%;
    }
    .MuiFab-extended.MuiFab-sizeMedium{
        width:90%;
        outline-style:none;
        margin:20px;
        // background:#3aac68;
    }
    .extra-taxes{
        display:flex;
        margin:10px;
        padding:0 60px;
        justify-content:space-between;
    }
`
const CouponWrapper = styled.div`
width:100%;
input{
    margin:20px auto;
    width:65%;
    border:none;
    padding:20px;
    outline-style:none;
    font-size:16px;
    text-align:center;
    // background:rgba(0,0,0,0.05);
}
`

const EmptyCart = styled.div`
    // width:400px;
    // box-shadow: 0 0 2px 2px rgba(0,0,0,0.1);
    display:flex;
    justify-content:center;
    // margin:auto;
    flex-wrap:wrap;
    width:95%;
    border-radius:2px;
    margin:20px 10px;
    background:#fff;
    box-shadow:0 0 2px 2px rgba(0,0,0,0.05);
    .imgs{
        width:400px;
    }
    .headings{
        width:100%;
        padding:10px 20px 0 20px;
        font-family: ProximaNova-Bold,Helvetica,Arial,sans-serif;
        font-weight: 600;
        font-size: 18px;
        color:#555;
        text-align:center;
    }
`