// // import React from 'react';
// // import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
// // import logo from '../images/logo.png';
// // import NavLink from 'react-bootstrap/NavLink';
// // import { Link } from 'react-router-dom';
// // import styled from 'styled-components';
// // import { IoMdCart } from 'react-icons/io';
// // import { MdRestaurantMenu } from 'react-icons/md';

// // export default class Navigation extends React.Component {
// //   logout = () => {
// //     localStorage.clear();
// //     setTimeout(function () {
// //       window.location.reload()
// //     }, 100);
// //   }
// //   render() {
// //     return (
// //       <NavbarWrapper>
// //         <Navbar collapseOnSelect fixed="top" expand="lg" className="navBg">
// //           <Navbar.Brand>
// //             <Link to="/">
// //               <img
// //                 alt=""
// //                 src={logo}
// //                 className="d-inline-block align-top logo"
// //               />
// //             </Link>
// //           </Navbar.Brand>
// //           <Navbar.Toggle className="btn-color" />
// //           <Navbar.Collapse className="justify-content-end">
// //             <Nav>
// //               {localStorage.getItem("loggedin") ?
// //                 <NavDropdown title={localStorage.getItem("user").split(" ")[0]} id="collasible-nav-dropdown" className="user-drop">
// //                   <NavDropdown.Item>
// //                     <Link to="/profile" className="linked">Profile</Link>
// //                   </NavDropdown.Item>
// //                   <NavDropdown.Item>
// //                     <Link to="/settings" className="linked">Setting</Link>
// //                   </NavDropdown.Item>
// //                   <NavDropdown.Divider />
// //                   <NavDropdown.Item>
// //                     <Link to="/" className="linked" onClick={this.logout}>Logout</Link>
// //                   </NavDropdown.Item>
// //                 </NavDropdown> : null
// //               }
// //               <NavLink>
// //                 <Link to="/order" className="links"><IoMdCart /> Order Food</Link>
// //               </NavLink>
// //               <NavLink>
// //                 <Link to="/booktable" className="links"><MdRestaurantMenu /> Book a Table</Link>
// //               </NavLink>
// //             </Nav>
// //           </Navbar.Collapse>
// //         </Navbar>
// //       </NavbarWrapper>
// //     );
// //   }
// // }

// // const NavbarWrapper = styled.div`
// // .navBg{
// //   background:#000;
// // }
// //   .links{
// //     color:rgba(255,255,255,0.5);
// //   }
// //   .linked{
// //     color:#000;
// //     text-decoration:none;
// //   }
// //  .links:hover{
// //    text-decoration:none;
// //    color:rgba(255,255,255,0.9);
// //  }
// //  .btn-color{
// //    background:rgba(255,255,255,0.5);
// //    outline-style:none;
// //   }
// //   .btn-color:hover{
// //    background:rgba(255,255,255,0.9)!important;    
// //   }
// //  .logo{
// //    height:50px;
// //    width:50px;
// //  }
// //  .user-drop{
// //    border:2px solid rgba(108, 156, 237);
// //    border-radius:5px;
// //    font-family:"Times New Roman";
// //    text-transform:capitalize;
// //  }
// //  .user-drop:hover{
// //    border:2px solid rgba(42, 105, 211);
// //  }
// //  .user-drop .nav-link{
// //    color:#f3f3f3 !important;
// //  }
// //  .user-drop .nav-link:hover{
// //    color:rgba(255,255,255,0.5) !important;
// //  }
// //  @media only screen and (max-width:768px){
// //    .logo{
// //      width:60px;
// //      height:60px;
// //    }
// //    .nav-link{
// //      margin-left:10px;
// //    }
// //  }
// // `

import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import styled from 'styled-components';
import Slide from '@material-ui/core/Slide';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import RestaurantMenuIcon from '@material-ui/icons/RestaurantMenu';
import AccountCircleSharpIcon from '@material-ui/icons/AccountCircleSharp';
import { FaSignInAlt } from 'react-icons/fa'
import Button from '@material-ui/core/Button';
import FilterListOutlinedIcon from '@material-ui/icons/FilterListOutlined';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import { ModalContext } from '../contexts/ModalContext';
import Badge from '@material-ui/core/Badge';

export default class Navbar extends React.Component {
  constructor() {
    super()
    this.state = {
      anchorEl: null,
      setAnchorEl: null
    }
  }
  HideOnScroll = (props) => {
    const { children, window } = props;
    const trigger = useScrollTrigger({ target: window ? window() : undefined });
    return (
      <Slide appear={false} direction="down" in={!trigger}>
        {children}
      </Slide>
    );
  }

  handleClick = (event) => {
    this.setState({ setAnchorEl: event.currentTarget, anchorEl: true })
  }

  handleClose = () => {
    this.setState({ setAnchorEl: null, anchorEl: false })
  }
  render() {
    return (
      <ModalContext.Consumer>
        {
          value => {
            const { category, openCart, cartItem, searchMenuItem, handleModal } = value;
            return (
              <NavbarWrapper>
                <AppBar color="white">
                  {/* <this.HideOnScroll {...this.props}> */}
                  <Toolbar className="top-menu">
                    <Typography variant="h6" color="inherit">
                      <RestaurantMenuIcon color="primary" className="icons" /> EAT N FEEL
                        </Typography>
                    <div className="right-menu">
                      <SearchOutlinedIcon className="icons-menu" />
                      <input type="text" className="search-item" placeholder="Search Food" onChange={searchMenuItem} />
                      <Button>
                        <FilterListOutlinedIcon className="icons-menu" />
                      </Button>
                      <Button onClick={openCart}>
                        <Badge badgeContent={cartItem.length} color="primary">
                          <ShoppingCartOutlinedIcon className="icons-menu" />
                        </Badge>
                      </Button>
                      <Button
                        aria-controls="simple-menu"
                        aria-haspopup="true"
                        onClick={this.handleClick}
                        hidden={localStorage.getItem('loggedin') == null || localStorage.getItem('loggedin') == true ? true : false}
                      >
                        <AccountCircleSharpIcon
                          className="icons-menu"
                        />
                      </Button>
                      <Menu
                        id="simple-menu"
                        anchorEl={Menu}

                        open={Boolean(this.state.anchorEl)}
                        onClose={this.handleClose}
                        getContentAnchorEl={null}
                        anchorOrigin={{
                          vertical: 'top',
                          horizontal: 'right',
                        }}
                        transformOrigin={{
                          vertical: 'top',
                          horizontal: 'right',
                        }}
                      >
                        <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                        <MenuItem onClick={this.handleClose}>My Orders</MenuItem>
                        <MenuItem onClick={this.handleClose}>Logout</MenuItem>
                      </Menu>

                      <Button
                        hidden={localStorage.getItem('loggedin') == null || localStorage.getItem('loggedin') == true ? false : true}
                        onClick={handleModal}
                      >
                        <FaSignInAlt />
                      </Button>
                    </div>
                  </Toolbar>
                  {/* </this.HideOnScroll> */}
                  <Toolbar >
                    <ul className="menu-items">
                      {category.map((data, key) => {
                        return <>
                          <li>{data}</li>
                        </>
                      })}
                    </ul>
                  </Toolbar>
                </AppBar>
              </NavbarWrapper>
            );
          }
        }
      </ModalContext.Consumer>
    )
  }
}

const NavbarWrapper = styled.div`
            flex-grow:1;
            .MuiTypography-h6{
                display:flex;
                align-items:center;
            }
            .MuiToolbar-regular{
                min-height:50px;
                border-bottom:1px solid rgba(0,0,0,0.1);
            }
            .icons{
                padding-right:5px;
            }
            .icons-menu{
                color:#666;
                padding:0 5px;
            }
            .top-menu{
                display:flex;
                justify-content:space-between;
                align-items:center;
            }
            .MuiMenu-paper{
                top:50px !important;
            }
            .right-menu{
                display: flex;
                align-items: center;
            }
            .search-item{
              border:none;
              border-bottom:1px solid rgba(0,0,0,0.1);
              width:200px;
              font-size:12px;
              padding:5px 10px;
              outline-style:none;
            }
            .menu-items{
                width:100%;
                display: flex;
                flex-wrap: wrap;
                padding: 0;
                text-transform: capitalize;
            }
            li{
                list-style:none;
                margin:0 20px;
                color:#333;   
            }
        `