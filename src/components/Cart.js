import { ModalContext } from '../contexts/ModalContext';
import styled from 'styled-components';
import React from 'react';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Fab from '@material-ui/core/Fab';
import { FaCartArrowDown, FaRupeeSign } from 'react-icons/fa';
import { IoIosRestaurant } from 'react-icons/io';
import img from '../images/food.jpg'
import empty from '../images/emptycart.png';

export default class Cart extends React.Component {
    render() {

        const Transition = React.forwardRef(function Transition(props, ref) {
            return <Slide direction="up" ref={ref} {...props} />;
        });
        return (
            <ModalContext.Consumer>
                {
                    value => {
                        const { cartOpen, closeCart, cartItem, decrementItem, incrementItem, cartTotal,placeOrder } = value;
                        return (
                            <div>
                                <CartWrapper>
                                    <Dialog fullScreen open={cartOpen} onClose={closeCart} TransitionComponent={Transition}>
                                        <AppBar className="appBar" color="default">
                                            <Toolbar >
                                                <IconButton edge="start" color="default" onClick={closeCart} aria-label="close">
                                                    <CloseIcon />
                                                </IconButton>
                                                Cart Total : <FaRupeeSign />{cartTotal}
                                            </Toolbar>
                                        </AppBar>
                                        <CartContent hidden={cartTotal <= 0 ? true : false}>
                                            <YourCart >
                                                <div className="headings">Your Cart</div>
                                                <div className="items"><FaCartArrowDown style={{ marginRight: "5px" }} /> {cartItem.length} Items</div>
                                                {cartItem.map((data, key) => {
                                                    return (
                                                        <div className="cart-list">
                                                            <div className="list-item" style={{ width: "10%" }} hidden={data.food_type === 'veg' ? false : true}>
                                                                <div className='veg-icons'><span className="veg-dot"></span></div>
                                                            </div>
                                                            <div className="list-item" style={{ width: "10%" }} hidden={data.food_type === 'non-veg' ? false : true}>
                                                                <div className='non-veg-icons'><span className="non-veg-dot"></span></div>
                                                            </div>
                                                            <div className="list-item" style={{ width: "60%" }}>{data.menu_name}</div>
                                                            {/* <div className="list-item" style={{ width: "20%" }}><FaRupeeSign/> 120 </div> */}
                                                            <div className="list-item" style={{ width: "20%" }}>
                                                                <ButtonGroup size="small" aria-label="small outlined button group">
                                                                    <Button onClick={() => decrementItem(data.menuId)}>-</Button>
                                                                    <Button>{data.quantity}</Button>
                                                                    <Button onClick={() => incrementItem(data.menuId)}>+</Button>
                                                                </ButtonGroup>
                                                            </div>

                                                            <div className="list-item price"><FaRupeeSign />{data.menu_price * data.quantity} </div>
                                                        </div>)
                                                })}
                                                <div>
                                                    <Fab variant="extended" color="primary" aria-label="add" size="medium" onClick={placeOrder}>
                                                        Place Order <FaRupeeSign />{cartTotal}
                                                    </Fab>
                                                </div>

                                            </YourCart>
                                            <YourCart>
                                                <div className="headings">You May Also Like</div>
                                                <div className="items"><IoIosRestaurant style={{ marginRight: "5px" }} /> Feature Dishes</div>
                                                <div className="items-likes">
                                                    <img src={img} alt="img" className="imgs" />
                                                    <div className="cart-list items-likes-content">
                                                        <div className="list-item" style={{ width: "2%" }}>
                                                            <div className='veg-icons'><span className="veg-dot"></span></div>
                                                        </div>
                                                        <div className="list-item" style={{ width: "60%" }}> Paneer Manchuriyan</div>
                                                        <div className="list-item" style={{ width: "20%" }}>
                                                            <Button color="default" variant="contained">Add</Button>
                                                        </div>
                                                        <div className="list-item price" style={{ padding: "0" }}><FaRupeeSign />120 </div>
                                                    </div>
                                                </div>
                                            </YourCart>
                                        </CartContent>
                                        <EmptyCart hidden={cartTotal > 0 ? true : false}>
                                            <img className="imgs" src={empty} alt="empty cart" />
                                            <div className="headings">
                                                Your Cart is Epmty
                                            </div>
                                            <p>Add some of our delicious food items to your cart :)</p>
                                        </EmptyCart>
                                    </Dialog>
                                </CartWrapper>
                            </div>
                        )
                    }
                }
            </ModalContext.Consumer>
        )
    }
}

const CartWrapper = styled.div`
.appBar{
    position:relative;
}
`
const CartContent = styled.div`
    margin-top:40px;
    display:flex;
    justify-content:center;
`
const YourCart = styled.div`
margin:40px 20px;
    width:40%;
    border-radius:4px;
    .headings{
        width:100%;
        padding:10px 20px 0 20px;
        font-family: ProximaNova-Bold,Helvetica,Arial,sans-serif;
        font-weight: 700;
        font-size: 20px;
        color:#555;
    }
    .items{
        font-size:12px;
        padding:5px 20px;
        font-family:ProximaNova-Bold,Helvetica,Arial,sans-serif;
        color:#555;
        display:flex;
        align-items:center;
        border-bottom:1px solid rgba(0,0,0,0.05);
    }
    .veg-icons{
        width:11px;
        height:11px;
        position:absolute;
        border:1px solid green;
    }
    .veg-dot{
            width:5px;
            height:5px;
            border-radius:50%;
            background:green;
            margin:2px;
        position:absolute;
    }
    .non-veg-icons{
        width:11px;
        height:11px;
        position:absolute;
        border:1px solid red;
    }
    .non-veg-dot{
            width:5px;
            height:5px;
            border-radius:50%;
            background:red;
            margin:2px;
        position:absolute;
    }
    .cart-list{
        display:flex;
        flex-wrap:wrap;
        align-items:center;
        padding:10px 20px;
        border-bottom:1px solid rgba(0,0,0,0.05);
    }
    .list-item{
        font-size:16px;
        color:#333;
        margin:0 5px;
        text-transform:capitalize;
    }
    .price{
        font-size:18px;
        font-weight:700;
        width:100%;
        padding:0 55px;
    }
    .MuiButtonGroup-grouped {
        min-width: 30px;
        border-radius: 20px;
        padding:2px;
        outline-style:none;
    }
    .imgs{
        width:100px;
        border-bottom: 1px solid rgba(0,0,0,0.05)
    }
    .items-likes{
        display: flex;
        align-items: center;
    }
    .items-likes-content{
        border:none;
        width:100%;
    }
    .MuiFab-extended.MuiFab-sizeMedium{
        width:90%;
        outline-style:none;
        margin:20px;
        background:#3aac68;
    }
`
const EmptyCart = styled.div`
    width:400px;
    display:flex;
    justify-content:center;
    margin:auto;
    flex-wrap:wrap;
    .imgs{
        width:400px;
    }
    .headings{
        width:100%;
        padding:10px 20px 0 20px;
        font-family: ProximaNova-Bold,Helvetica,Arial,sans-serif;
        font-weight: 600;
        font-size: 18px;
        color:#555;
        text-align:center;
    }
`


