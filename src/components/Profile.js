import React from 'react';
import styled from 'styled-components';
import { Container, Row, Col, Nav } from 'react-bootstrap';
import pic from '../images/food1.png';
import { Link } from 'react-router-dom'

export default class Profile extends React.Component {
  render() {
    return (
      <ProfileWrapper>
        <Container>
          <Row className="justify-content-md-center profiler">
            <Col md="auto" className="marginer">
              <img src={pic} alt="profile pic" className="img" />
            </Col>
            <Col md="auto">
              <span>Hello,</span>
              <h1>{localStorage.getItem("user")}</h1>
            </Col>
          </Row>
          <Row>
            <Col sm={12}>
              <div className="profiler">
                <Nav>
                  <Nav.Link>
                    <Link to="/profile/orders" className="linked">Orders</Link>
                  </Nav.Link>
                  <Nav.Link>
                    <Link to="/profile/review" className="linked">Reviews</Link>
                  </Nav.Link>
                </Nav>
              </div>
            </Col>
          </Row>
        </Container>
      </ProfileWrapper>
    );
  }
}

const ProfileWrapper = styled.div`
  margin-top:90px;
  .profiler{
    background:#fff;
    box-shadow:0 0 2px 2px rgba(0,0,0,0.05);
    border-radius:5px;
    display:flex;
    align-items:center;
    flex-wrap:nowrap;
    margin:5px 0px;
  }
  .profiler span{
    width:100%;
    margin-top:10px;
    text-align:center;
  }
  .profiler h1{
    text-transform:capitalize;
    font: 600 30px 'Times New Roman';
  }
  .img{
    width:100px;
    height:100px;
    border-radius:50%;
    box-shadow:0 0 2px 2px rgba(0,0,0,0.1);
    margin:20px auto;
  }
  .padder{
    margin-right:10px;
    border-right:5px solid #f2f2f2;
  }
  .linked{
    color:rgba(0,0,0,0.8);
    text-decoration:none;
    font:14px "Times New Roman";
  }
 .linked:hover{
   color:rgba(0,0,0,0.7);
 }
  @media only screen and (max-width:768px){
    .marginer{
      display:flex;
      align-items:center;
    }
  }
`