import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import HomePage from './components/HomePage';
import SignInUp from './components/SignInUp';
import Navigation from './components/Navbar';
import Menu from './components/Menu';
import Profile from './components/Profile';
import ProfileOrders from './components/ProfileOrders';
import ProfileReview from './components/ProfileReview';
import Cart from './components/Cart';
import CheckOut from './components/CheckOut';

function App() {
  return (
    <div className="App">
      <Navigation />
      <Switch>
        <Route path="/" exact component={Menu} />
        {/* <Route path="/order" exact component={Menu} /> */}
        <Route path="/profile" exact render={
          () => (localStorage.getItem('loggedin') ? <Profile /> : <Redirect to="/" />)
        } />}
        <Route path="/profile/orders" render={
          () => (localStorage.getItem('loggedin') ? <ProfileOrders /> : <Redirect to="/" />)
        } />
        <Route path="/profile/review" render={
          () => (localStorage.getItem('loggedin') ? <ProfileReview /> : <Redirect to="/" />)
        } />
      </Switch>
      <SignInUp />
      <Cart />
      <CheckOut/>
    </div>
  );
}

export default App;
