export const profileOrder = () => {
  return fetch("http://localhost:3000/orders/" + localStorage.getItem('userId'), {
    method: "get",
    headers: {
      "authorization": "Bearer " + localStorage.getItem('token')
    }
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}

export const profileReview = () => {
  return fetch("http://localhost:3000/review/" + localStorage.getItem("userId"), {
    method: "get",
    headers: {
      "authorization": "Bearer " + localStorage.getItem('token')
    }
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}