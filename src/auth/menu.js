export const menu = () => {
  return fetch('http://localhost:3000/menu', {
    method: "get"
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}

export const menuSelect = (menuId, data) => {
  return fetch('http://localhost:3000/menu/select/' + menuId, {
    method: "put",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  })
    .then(response => response.json())
    .catch(error => console.log(error));
}